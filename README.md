# Usegalaxy.fr data manager

This repository installs and runs Galaxy data managers on [usegalaxy.fr](https://usegalaxy.fr/)

It is based on the Ansible role [galaxyproject/galaxy-tools](https://github.com/galaxyproject/ansible-galaxy-tools), and almost follows the usegalaxy.eu process [usegalaxy-eu/usegalaxy-eu-tools](https://github.com/usegalaxy-eu/usegalaxy-eu-tools).

## How to contribute

If you need a data manager that is not yet installed on usegalaxy.fr, you can freely contribute to this repository.

The list of tools is contained in a `.yml` file in the [`files/data_manager_tools` directory](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/data_manager/-/tree/master/files/data_manager_tools/). The `.yml.lock` files contains the list of specific versions of tools that are installed on usegalaxy.fr, they are managed automatically, you most likely don't need to modify them.

## Install a new tool

1. Add your data manager in the `files/data_manager_tools/data_manager.yml` file (you can use the built-in editor of GitLab, or edit on a local clone)
3. Create a Merge Request to propose your new tool

Example to add the data manager `data_manager_mothur_toolsuite` from the `iuc` ToolShed user:

[files/data_manager_tools/data_manager.yml](files/data_manager_tools/data_manager.yml)
```yaml
tools:
- name: data_manager_mothur_toolsuite
  owner: iuc
  tool_panel_section_label: Data Managers
```

Once your merge request is accepted, the data manager will be added to this repository, but will not be installed immediately on usegalaxy.fr. It will only be installed automatically during the next weekly automatic tool update (see below), which will automatically fill the `.yml.lock` file with the latest tool version.

## Updating an existing tool

Each week, an automatic process updates all the tools installed in this repository. A manual update can be forced by running manually the scheduled CI task.

## Running data manager

You can list specific runs of Data managers by filling the [files/data_manager_run/run_data_managers.yml](files/data_manager_run/run_data_managers.yml).

This is currently not run automatically as it would potentially add duplicate entries into data tables on each run.

Data managers need to be run manually by admins (except for data on CVMFS).
