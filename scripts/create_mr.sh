#/bin/bash

set -u # stop if a variable is not initialized
set -e # stop in case of error

date=$(date '+%Y-%m-%d_%H-%M-%S')
date_human=$(date '+%Y-%m-%d')

if git diff-index --quiet HEAD --; then
    echo "No changes detected, will not create a merge request."
    exit;
fi

url_host=`git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g"`
git remote set-url origin "https://gitlab-ci-token:${PRIVATE_TOKEN}@${url_host}"

git checkout -b "auto-$date"
git add .
git commit -m "Automatic update of data managers on $date_human"
git push --set-upstream origin "auto-$date"


# This code is from https://about.gitlab.com/blog/2017/09/05/how-to-automatically-create-a-new-mr-on-gitlab-with-gitlab-ci/

HOST="https://gitlab.com/api/v4/projects/"

# Look which is the default branch
TARGET_BRANCH=`curl --silent "${HOST}${CI_PROJECT_ID}" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" | python3 -c "import sys, json; print(json.load(sys.stdin)['default_branch'])"`;

# The description of our new MR, we want to remove the branch after the MR has
# been closed
BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"auto-${date}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": true,
    \"description\": \"This is an automatic update of data managers triggered by the IFB Bot, on ${date_human}.\",
    \"title\": \"[BOT] Automatic data manager list sync ${date_human}\"
}";

# Require a list of all the merge request and take a look if there is already
# one with the same source branch
LISTMR=`curl --silent "${HOST}${CI_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}"`;
COUNTBRANCHES=`echo ${LISTMR} | grep -o "\"source_branch\":\"auto-${date}\"" | wc -l`;

# No MR found, let's create a new one
if [ ${COUNTBRANCHES} -eq "0" ]; then
    curl -X POST "${HOST}${CI_PROJECT_ID}/merge_requests" \
        --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}";

    echo "Opened a new merge request: [BOT] Automatic data manager list sync ${date_human} and assigned to you";
    exit;
fi

echo "No new merge request opened";
